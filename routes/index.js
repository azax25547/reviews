const express = require('express')
const { Review } = require('../models/review');
const { HTTPError } = require('@azaxxc/common/src/errors/Http-error');

const router = express.Router();
// middlewares.authAPIs,
router.get('/api/reviews', async (req, res, next) => {
    const { userId } = req.query;
    try {
        const reviews = await Review.find({ userId }).populate('user').populate('recipe');
        if (!reviews)
            throw new HTTPError("Unable to fetch data from DB")

        res.send(reviews);
    } catch (err) {
        console.log(err)
        next(err)
    }

})


module.exports = { indexReviewRoute: router }