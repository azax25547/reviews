const router = require("express").Router();
const { middlewares } = require("@azaxxc/common/src");

const { Review } = require("../models/review");


router.post('/api/reviews',
    middlewares.authAPIs,
    async (req, res, next) => {
        const { userId, type, stars, description, likes,dislikes, recipeId } = req.body;
        let newReview;
        try {
            // let review = await Review.findById(reviewId);
            // if (review)
            //     throw new HTTPError("Review Not found!");

            newReview = await Review.create({
                type,
                stars,
                description,
                likes,
                dislikes,
                user: userId,
                recipe: recipeId
            })

            await newReview.save();
            res.status(201).send(newReview);
        } catch (err) {
            next(err)
        }

    })

module.exports = { createReviewRouter: router };