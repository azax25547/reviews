const express = require('express')
const { Review } = require('../models/review');
const { middlewares } = require("@azaxxc/common/src");
const router = express.Router();

router.put('/api/reviews/update/:id', middlewares.authAPIs, async (req, res, next) =>{
    let { id } = req.params;
    let { ...details } = req.body;

    // search for the review
    try {
        const rev = await Review.findByIdAndUpdate(id, {
            ...details
        }, {
            useFindAndModify: false,
            new: true
        })

        res.status(201).send(rev);
    } catch(err) {
        next(err);
    }

})

module.exports = { updateReviewRouter:router };