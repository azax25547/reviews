const {Recipe} = require("../../models/recipe");
const {User} = require("../../models/user");
const request = require("supertest");
const {app} = require("../../app");

const user = global.signin();


it('should update description for a specific review', async function () {
    const recipe = await Recipe.create({
        price: 100,
        quantity: 2,
        recipe: 'Biriyani'
    })

    const sampleUser = await User.create({
        firstname: 'Handi',
        lastname: 'Chandi'
    })

    await recipe.save();
    await sampleUser.save();

    const {body:reviewSample} = await request(app)
        .post('/api/reviews')
        .set('x-access-token', user['x-access-token'])
        .send({
            userId: sampleUser.id,
            type: "Dining",
            recipeId: recipe.id,
            description: "Item did not delivered in time"
        })
        .expect(201)

    const {body:response} = await request(app)
        .put(`/api/reviews/update/${reviewSample.id}`)
        .set('x-access-token', user['x-access-token'])
        .send({
            description: "Item is fine"
        }).expect(201)

});