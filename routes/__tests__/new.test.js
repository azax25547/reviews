const request = require("supertest");
const { app } = require("../../app");
const { Recipe } = require("../../models/recipe");
const { User } = require("../../models/user");

const user = global.signin();

it("creates an Review", async () => {
    const recipe = await Recipe.create({
        price: 100,
        quantity: 2,
        recipe: 'Biriyani'
    })

    const sampleUser = await User.create({
        firstname: 'Handi',
        lastname: 'Chandi'
    })

    await recipe.save();
    await sampleUser.save();

    await request(app)
        .post('/api/reviews')
        .set('x-access-token', user['x-access-token'])
        .send({
            userId: sampleUser.id,
            type: "Dining",
            recipeId: recipe.id,
            description: "Item did not delivered in time"
        })
        .expect(201)
})
