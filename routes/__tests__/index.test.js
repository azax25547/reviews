const request = require("supertest");
const { app } = require("../../app");
const { User } = require("../../models/user");
const { Recipe } = require("../../models/recipe");

const buildUser = async () => {
    const user = await User.create({
        firstname: 'John',
        lastname: 'Doe',
    })
    await user.save();
    return user;

}

const buildRecipe = async () => {
    const recipe = await Recipe.create({
        price: 100,
        quantity: 2,
        recipe: 'Biriyani'
    })

    await recipe.save();
    return recipe;
}

it('fetches reviews for an particular user', async () => {
    // create three recipes
    const sampleUser = await buildUser();
    const recipeOne = await buildRecipe();
    const userOne = global.signin();

    const { body: newReview } = await request(app)
        .post('/api/reviews')
        .set('x-access-token', userOne['x-access-token'])
        .send({
            userId: sampleUser.id,
            type: "Dining",
            stars: 4,
            description: "Nice",
            likes: 1,
            dislikes: 2,
            recipeId: recipeOne.id
        }).expect(201);

    const { body: reviewOg } = await request(app)
        .get('/api/reviews')
        .query({
            userId: sampleUser.id
        })


    expect(reviewOg[0].user.id).toBe(sampleUser.id)
    // expect(reviewOg[1].userId).toBe(orderTwo.userId)

})

it("fetches the reviews for a particular recipe",async () => {
    const recipeOne = await buildRecipe();
    const sampleUser = await buildUser();
    const userOne = global.signin();

    await request(app)
        .post('/api/reviews')
        .set('x-access-token',userOne['x-access-token'])
        .send({
            userId: sampleUser.id,
            type: "Delivery",
            description: "Nice taste",
            recipeId: recipeOne.id
        }).expect(201);

    const { body: reviewOg } = await request(app)
        .get('/api/reviews')
        .query({
            userId: sampleUser.id
        })


    expect(reviewOg[0].user.id).toBe(sampleUser.id)
    expect(reviewOg[0].recipe.id).toBe(recipeOne.id)
})