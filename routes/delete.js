const express = require("express");
const { middlewares } = require('@azaxxc/common/src');

const router = express.Router();
const { Review } = require('../models/review');

router.delete('/api/reviews/delete/:id', middlewares.authAPIs, async (req, res, next) => {

    const { id } = req.params;
    try {
        const deletedReviews = await Review.deleteMany({
            id
        })

        res.status(201).send(deletedReviews);
    } catch (err) {
        next(err);
    }

})

module.exports = {
    deleteReviewRouter: router
}