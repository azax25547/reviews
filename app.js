const express = require('express');
const { json } = require('body-parser');
const session = require("express-session");
const cors = require('cors');
const { middlewares } = require('@azaxxc/common/src');
require("dotenv").config();

const { createReviewRouter } = require('./routes/new')
const { indexReviewRoute } = require('./routes/index')
const { updateReviewRouter } = require('./routes/update')
const { deleteReviewRouter } = require('./routes/delete')

const app = express();
app.set('trust proxy', true);
app.use(json())

app.use(cors(
    {
        origin: "http://localhost:3000", // allow to server to accept request from different origin
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
        credentials: true, // allow session cookie from browser to pass through
    }
))

app.use(session({
    cookie: { maxAge: 60000000 },
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false
}))

//routes

// app.get('/status', middlewares.authAPIs, (req, res) => {
//     res.json('HellO There');
// })
//
app.use(createReviewRouter);
app.use(indexReviewRoute);
app.use(updateReviewRouter);
app.use(deleteReviewRouter);
// app.all('*', middlewares.errorhandler)

app.use(middlewares.errorhandler);

module.exports = { app };
