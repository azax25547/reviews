const mongoose = require('mongoose');

const ReviewSchema = new mongoose.Schema({
    type: String,
    stars: {
        type: Number,
        default: 3
    },
    description: String,
    likes: {
        type: Number,
        default: 0
    },
    dislikes: {
        type: Number,
        default: 0
    },
    img: {
        type: [String],
        default: []
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    recipe : {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Recipe"
    }
}, {
    toJSON: {
        transform(doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
        }
    }
})

const Review = new mongoose.model("Review", ReviewSchema)

module.exports = {
    Review
}